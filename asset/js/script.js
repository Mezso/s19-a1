  let student1 = {
 	name: "Shawn Michaels",
 	birthday: "May 5, 2003",
 	age: 18,
 	isEnrolled: true,
 	classes: ["Philosphy 101","Social Sciences 201"]
 }

 let student2 = {
 	name : "Steve Austin",
 	birthday: "June 15, 2001",
 	age: 20,
 	isEnrolled: true,
 	classes: ["Philosphy 401", "Natural Sciences 402"]
 }


 const introduce = (student) => {

	//Note: You can destructure objects inside functions.

	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses " + student.classes);
}

const getCube = (num) => {

	console.log(Math.pow(num,3));
	return Math.pow(num,3);

}
introduce(student1)

let cube =  getCube(3);
console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})
let numsSquared = numArr.map(function(num){

	return Math.pow(num,2);

})
console.log(numsSquared);

class Doggy {
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7 ;
	}
}

let dog1 = new Doggy('Pabibo', 'German Sherperd', 2);
let dog2 = new Doggy('Chocoloco', 'Dalmatian', 1);
console.log(dog1)
console.log(dog2)